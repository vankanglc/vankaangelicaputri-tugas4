import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {CheckBox} from 'react-native-elements';

import {
    View, 
    Text, 
    Image,
    ScrollView,
    KeyboardAvoidingView,
    TextInput, 
    TouchableOpacity, 
    StyleSheet,
    Dimensions
} from 'react-native'

const Transaksi = ({
    navigation,
    route
 }) => {
    return(
       <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
          <ScrollView //component yang digunakan agar tampilan kita bisa discroll
             showsVerticalScrollIndicator={false}
             contentContainerStyle={{paddingBottom: 10}}>
            <View style= {{marginHorizontal: 20,}}>
                <TouchableOpacity style={styles.kotakKeranjang}
                    onPress={() => navigation.navigate('DetailTransaksi')}>
                    <View style={{
                        width: '80%',
                        marginLeft: 15,
                        paddingVertical: 23, 
                        justifyContent: 'space-evenly',
                    }}>
                    <Text style={{color: '#737373'}}>
                        20 Desember 2020    09:00 </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        New Balance - Pink Abu - 40
                    </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        Cuci Sepatu
                    </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        Kode Reservasi
                    </Text>
                    </View>
                </TouchableOpacity>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 40,
                }}>
                    </View>
                </View>            
          </ScrollView>
       </View>
    )
 }

 const styles = StyleSheet.create({
    kotakKeranjang: {
        backgroundColor: '#fff',
        borderRadius: 9,
        marginBottom: 5,
        marginTop: 9,
        elevation: 3,
    },
 })

 export default Transaksi;