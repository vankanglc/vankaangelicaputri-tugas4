import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {CheckBox} from 'react-native-elements';

import {
    View, 
    Text, 
    Image,
    ScrollView,
    KeyboardAvoidingView,
    TextInput, 
    TouchableOpacity, 
    StyleSheet,
    Dimensions
} from 'react-native'

const Berhasil = ({
    navigation,
    route
 }) => {
    return(
       <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
          <ScrollView //component yang digunakan agar tampilan kita bisa discroll
             showsVerticalScrollIndicator={false}
             contentContainerStyle={{paddingBottom: 10}}>
            <View style= {{marginHorizontal: 20, justifyContent:'center', alignItems: 'center'}}>
            <Text style={{
                fontSize: 16, 
                fontWeight: 'bold'
            }}>
                Reservasi Berhasil
            </Text>
            <Image source={require('../assets/image/TickSquare.png')}
                    style={{
                        width: 84,
                        height: 84,
                        marginVertical: 24,
                    }}/>
            <Text style={{
                fontSize: 16, 
                fontWeight: 'bold'
            }}>
                 Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
            </Text>
                   <TouchableOpacity onPress={() => navigation.navigate('Transaksi')}
                      style={{
                         width: '100%',
                         marginTop: 30,
                         backgroundColor: '#BB2427',
                         borderRadius: 8,
                         paddingVertical: 15,
                         justifyContent: 'center',
                         alignItems: 'center',
                         marginBottom: 35,
                      }}>
                      <Text style={{
                         color: '#fff', 
                         fontSize: 16, 
                         fontWeight: 'bold'
                      }}>
                         Lihat Kode Reservasi
                      </Text>
                   </TouchableOpacity >
                </View>            
          </ScrollView>
       </View>
    )
 }

 export default Berhasil;