import {
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View,
    Modal,
    Button,
    TouchableOpacity,
    Dimensions,
    FlatList,
  } from 'react-native';
  import React, {useState} from 'react';
  import Icon from 'react-native-vector-icons/dist/Ionicons';
  
  const Home = ({navigation}) => {
    const [showModal, setShowModal] = useState(false);
  
    const SliderItem = ({item}) => {
      return (
        <TouchableOpacity
          // style={styles.card}
          onPress={() => navigation.navigate('Detail')}>
          <View style={styles.card}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 135,
                // backgroundColor: 'red',
              }}>
              <Image
                source={item.img}
                resizeMode="contain"
                style={{
                  height: '100%',
                  width: 100,
                  borderRadius: 15,
                  // width: 100,
                  // resizeMode: 'contain',
                }}
              />
              <View style={{justifyContent: 'center', marginHorizontal: 15}}>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="md-star" size={10} color="#FFC107" />
                  <Icon name="md-star" size={10} color="#FFC107" />
                  <Icon name="md-star" size={10} color="#FFC107" />
                  <Icon name="md-star" size={10} color="#FFC107" />
                  <Icon name="md-star-outline" size={10} color="#FFC107" />
                </View>
                <Text> {item.rating}</Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 16,
                    color: 'black',
                    paddingTop: 5,
                  }}>
                  {item.title}
                </Text>
                <Text
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{paddingTop: 5}}>
                  {item.address}
                </Text>
                <View
                  style={{
                    marginTop: 10,
                    backgroundColor:
                      item.status == 'BUKA' ? '#11A84E1F' : '#E64C3C33',
                    width: 58,
                    height: 21,
                    borderRadius: 20,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: item.status == 'BUKA' ? '#11A84E' : '#EA3D3D',
                    }}>
                    {item.status}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    };
  
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 10}}>
          <View style={{paddingHorizontal: 22, height: 290}}>
            <View style={styles.heading}>
              <Image
                style={{width: 45, height: 45, resizeMode: 'contain'}}
                source={require('../assets/image/foto.png')}
              />
              <Icon
                name="cart"
                size={25}
                color="black"
                style={styles.iconKeranjang}
                onPress={() => navigation.navigate('Keranjang')}
              />
            </View>
            <Text style={{color: 'black', fontSize: 15}}>Hello, Agil!</Text>
            <Text style={{color: 'black', fontSize: 25, fontWeight: 'bold'}}>
              Ingin merawat dan perbaiki sepatumu? cari disini
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 20,
                justifyContent: 'space-between',
              }}>
              <TextInput style={styles.search}>
                <Icon
                  name="search-outline"
                  size={25}
                  style={{paddingRight: 20}}
                />
              </TextInput>
              <Modal
                animationType={'slide'}
                transparent={true}
                visible={showModal}
                onRequestClose={() => {
                  console.log('Modal has been closed.');
                }}>
                {/*All views of Modal*/}
                {/*Animation can be slide, slide, none*/}
                <View style={styles.modal}>
                  <Text>Filter Search</Text>
                  <Button
                    title="OKE"
                    onPress={() => {
                      setShowModal(!showModal);
                    }}
                  />
                </View>
              </Modal>
              <TouchableOpacity
                style={styles.filter}
                onPress={() => {
                  setShowModal(!showModal);
                }}>
                <Icon name="filter" size={25} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.downpage}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 17,
              }}>
              <View style={styles.kategori}>
                <Image
                  source={require('../assets/icon/sepatu.png')}
                  style={{height: 45, width: 45}}
                />
                <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Sepatu</Text>
              </View>
              <View style={styles.kategori}>
                <Image
                  source={require('../assets/icon/jaket.png')}
                  style={{height: 45, width: 45}}
                />
                <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Jaket</Text>
              </View>
              <View style={styles.kategori}>
                <Image
                  source={require('../assets/icon/tas.png')}
                  style={{height: 45, width: 45}}
                />
                <Text style={{color: '#BB2427', fontWeight: 'bold'}}>Tas</Text>
              </View>
            </View>
            <View style={styles.rekomen}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'black'}}>
                Rekomendasi Terdekat
              </Text>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: '#BB2427'}}>
                View All
              </Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('')} 
                style={styles.kotakToko}>
                    <View style={{
                        flexDirection : 'row', justifyContent: 'space-between'}}>
                            <Image style={styles.imageToko}
                                source={require('../assets/image/toko2.png')}/>
                                <View style={{ marginLeft : 15, marginTop: 15, width: '60%'}}>
                                <Image style={{height: 9, width: 53}}
                                source={require('../assets/icon/rating.png')}/>
                                <Text style={styles.textRating}>
                                    4.8 Ratings
                                </Text>
                                <Text style={styles.textToko}>
                                    Jack Repair Gejayan
                                </Text>
                                <Text style={{fontSize: 9}}>
                                    Jl. Gejayan III No.2, Karangasem, Kec. Laweyan ...
                                </Text>
                                <Text style={styles.tokoTutup}>
                                    TUTUP
                                </Text>
                            </View>
                            <Image style={{marginTop: 15, marginRight: 15}}
                                source={require('../assets/icon/heart.png')}/>
                        </View>
            </TouchableOpacity >
            <TouchableOpacity onPress={() => navigation.navigate('Detail')} 
                style={styles.kotakToko}>
                    <View style={{
                        flexDirection : 'row', justifyContent: 'space-between'}}>
                            <Image style={styles.imageToko}
                                source={require('../assets/image/toko1.png')}/>
                                <View style={{ marginLeft : 15, marginTop: 15, width: '60%'}}>
                                <Image style={{height: 9, width: 53}}
                                source={require('../assets/icon/rating.png')}/>
                                <Text style={styles.textRating}>
                                    4.7 Ratings
                                </Text>
                                <Text style={styles.textToko}>
                                    Jack Repair Seturan
                                </Text>
                                <Text style={{fontSize: 9}}>
                                    Jl. Seturan Kec. Laweyan ...
                                </Text>
                                <Text style={styles.tokoBuka}>
                                    BUKA
                                </Text>
                            </View>
                            <Image style={{marginTop: 15, marginRight: 15}}
                                source={require('../assets/icon/heart.png')}/>
                        </View>
            </TouchableOpacity >
            <FlatList
              renderItem={({item}) => <SliderItem item={item} />}
              pagingEnabled
              snapToAlignment="center"
              showsHorizontalScrollIndicator={false}
              style={{width: '100%', height: '100%'}}
            />
          </View>
        </ScrollView>
      </View>
    );
  };
  
export default Home;
  
  const styles = StyleSheet.create({
    kotakToko: {
        backgroundColor: '#fff',
        marginHorizontal: 20,
        borderRadius: 9,
        marginBottom: 5,    
    },
    imageToko: {
        width: 100,
        height: 151,
        marginLeft: 6,
        marginVertical: 6,
    },
    textRating: {
        fontSize: 10, 
        paddingVertical: 5,
        color: '#D8D8D8',
    },
    textToko:{
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
        paddingBottom: 5,
    },
    tokoTutup:{
        backgroundColor: '#e64c3c33',
        color: '#EA3D3D',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 25,
        borderRadius: 10.5,
        width: 65,
        padding: 3,
    },
    tokoBuka: {
        backgroundColor: '#11a84e1f',
        color: '#11A84E',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 25,
        borderRadius: 10.5,
        width: 65,
        padding: 3,
    },
  });
  
  