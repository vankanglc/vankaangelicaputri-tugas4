import React, {useState} from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {CheckBox} from 'react-native-elements';

import {
    View, 
    Text, 
    Image,
    ScrollView,
    KeyboardAvoidingView,
    TextInput, 
    TouchableOpacity, 
    StyleSheet,
    Dimensions
} from 'react-native'

const Formulir = ({
    navigation,
    route
 }) => {
    const [isSelected, setSelection] = useState(false);
    const [isChecked, setCheck] = useState(false);

    return(
       <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
          <ScrollView //component yang digunakan agar tampilan kita bisa discroll
             showsVerticalScrollIndicator={false}
             contentContainerStyle={{paddingBottom: 50}}
          >
             <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                behavior='padding' //tampilan form atau text input
                enabled
                keyboardVerticalOffset={-500}
             >
                <View style={{                  
                   width: '100%',
                   backgroundColor: '#fff',
                   borderTopLeftRadius: 19,
                   borderTopRightRadius: 19,
                   paddingHorizontal: 20,
                   paddingTop: 38,
                   marginTop: -125
                }}>
                   <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Merek
                   </Text> 
                   <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                      placeholder='Masukkan Merk Barang' 
                      style={{
                         marginTop: 15,
                         marginBottom: 15,
                         width: '100%',
                         borderRadius: 8,
                         backgroundColor: '#F6F8FF',
                         paddingHorizontal: 10
                      }}
                    />
                    <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Warna
                   </Text> 
                   <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                      placeholder='Warna Barang, cth : Merah - Putih' 
                      style={{
                         marginTop: 15,
                         marginBottom: 15,
                         width: '100%',
                         borderRadius: 8,
                         backgroundColor: '#F6F8FF',
                         paddingHorizontal: 10
                      }}
                    />
                    <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Ukuran
                   </Text> 
                   <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                      placeholder='Cth : S, M, L / 39,40' 
                      style={{
                         marginTop: 15,
                         marginBottom: 15,
                         width: '100%',
                         borderRadius: 8,
                         backgroundColor: '#F6F8FF',
                         paddingHorizontal: 10
                      }}
                    />
                    <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Photo
                   </Text> 
                   <View style={{
                    width: '25%',
                    height: '10%',
                    borderRadius: 8,
                    borderWidth: 1,
                    borderColor: '#BB2427',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 11,
                   }}>
                    <Image 
                        source={require('../assets/icon/Camera.png')}
                        style={{
                            width: 20,
                            height: 18                         
                        }}></Image>
                      <Text style={{
                        color: '#BB2427',
                        fontSize: 12,
                        marginTop: 10,                    
                      }}>Add Photo</Text>
                   </View>
                   <CheckBox
                    title="Ganti Sol Sepatu"
                    checked={isSelected}
                    onPress={() => setSelection(!isSelected)}
                    checkedColor={'#BB2427'}
                    />
                    <CheckBox
                    title="Jahit Sepatu"
                    checked={isChecked}
                    onPress={() => setCheck(!isChecked)}
                    checkedColor={'#BB2427'}
                    />
                    <CheckBox
                    title="Repaint Sepatu"
                    checked={isSelected}
                    onPress={() => setSelection(!isSelected)}
                    checkedColor={'#BB2427'}
                    />
                    <CheckBox
                    title="Cuci Sepatu"
                    checked={isSelected}
                    onPress={() => setSelection(!isSelected)}
                    checkedColor="#BB2427"
                    />
                   <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Catatan
                   </Text> 
                   <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                      placeholder='Cth : ingin ganti sol baru' 
                      style={{
                        backgroundColor: '#F6F8FF',
                        width: '100%',
                        height: 100,
                        marginTop: 11,
                        paddingLeft: 10,
                        borderRadius: 10,
                      }}
                    />
                   <TouchableOpacity onPress={() => navigation.navigate('Keranjang')} 
                      style={{
                         width: '100%',
                         marginTop: 30,
                         backgroundColor: '#BB2427',
                         borderRadius: 8,
                         paddingVertical: 15,
                         justifyContent: 'center',
                         alignItems: 'center'
                      }}
                   >
                      <Text style={{
                         color: '#fff', 
                         fontSize: 16, 
                         fontWeight: 'bold'
                      }}>
                         Masukkan Keranjang
                      </Text>
                   </TouchableOpacity >
                   </View>            
             </KeyboardAvoidingView>
          </ScrollView>
       </View>
    )
 }
 export default Formulir;