import React from 'react'

import {
    View, 
    Text, 
    Image,
    ScrollView,
    KeyboardAvoidingView,
    TextInput, 
    TouchableOpacity, 
    StyleSheet,
    Dimensions
} from 'react-native'

const Detail = ({
    navigation,
    route
 }) => {
    return(
       <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
          <ScrollView //component yang digunakan agar tampilan kita bisa discroll
             showsVerticalScrollIndicator={false}
             contentContainerStyle={{paddingBottom: 10}}
          >
             <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                behavior='padding' //tampilan form atau text input
                enabled
                keyboardVerticalOffset={-500}
             >
                <Image
                   source={require('../assets/image/bgdetail.png')} //load atau panggil asset image dari local
                   style={{
                      width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
                      height: 317
                   }}
                />
                <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('BottomNav')}>
                <Image
                  source={require('../assets/icon/back.png')}
                  style={{
                    width: 24,
                    height: 24,
                    marginHorizontal: 26,
                    marginTop: 28,
                  }}></Image>
              </TouchableOpacity>
                <TouchableOpacity
                onPress={() => navigation.navigate('Keranjang')}>
                <Image
                  source={require('../assets/icon/Bag1.png')}
                  style={{
                    width: 24,
                    height: 24,
                    marginHorizontal: 26,
                    marginTop: 28,
                  }}></Image>
              </TouchableOpacity>
              </View>
                <View style={{                  
                   width: '100%',
                   backgroundColor: '#fff',
                   borderTopLeftRadius: 19,
                   borderTopRightRadius: 19,
                   paddingHorizontal: 20,
                   paddingTop: 38,
                   marginTop: -20
                }}>
                        <Text style={{fontWeight: 'bold', fontSize: 19, color: '#000'}}>
                            Jack Repair Seturan
                        </Text>
                        <Image style={{height: 9, width: 53}}
                                source={require('../assets/icon/rating.png')}/>
                        <View style={{
                            marginLeft: -4, 
                            marginTop: 13, 
                            flexDirection:'row', 
                            justifyContent: 'space-between'
                        }}>
                         <Image style={{height: 9, width: 53}}
                                source={require('../assets/icon/location_outline.png')}/> 
                        <View style={{width: '75%'}}>
                            <Text style={{color: '#979797', fontSize: 12, marginLeft: 7}}>
                                Jalan Affandi (Gejayan), No.15, Sleman Yogyakarta, 55384
                            </Text>
                            </View>  
                        <View>
                        <Text style={{fontSize: 12, fontWeight: 'bold', color: '#3471CD'}}>
                            Lihat Maps
                        </Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 13}}>
              <Text style={{
                backgroundColor: '#11a84e1f',
                color: '#11A84E',
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 25,
                borderRadius: 10.5,
                width: 65,
                padding: 3,
              }}>BUKA</Text>
              <Text style={{
                fontWeight: 'bold',
                color: '#343434',
                fontSize: 12,
                marginTop: 3,
                marginLeft: 15            
              }}>09:00 - 21:00</Text>
            </View>
          <View style={{
            borderColor: '#EEE',
            borderBottomWidth: 1,
            borderTopWidth: 1,
            marginTop: 13,        
          }}></View>
            <View>
              <Text style={{color: '#201F26', fontSize: 16, marginTop: 23,}}>
                Deskripsi</Text>
              <Text style={{color: '#595959', fontSize: 14,marginTop: 6,}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
                gravida mattis arcu interdum lectus egestas scelerisque. Blandit
                porttitor diam viverra amet nulla sodales aliquet est. Donec
                enim turpis rhoncus quis integer. Ullamcorper morbi donec
                tristique condimentum ornare imperdiet facilisi pretium
                molestie.
              </Text>
            </View>
            <View>
              <Text style={{color: '#201F26', fontSize: 16, marginTop: 23,}}>
                Range Biaya</Text>
              <Text style={{color: '#595959', fontSize: 14,marginTop: 6,}}>
                Rp 20.000 - 80.000</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Formulir')} 
                      style={{
                         width: '100%',
                         marginTop: 30,
                         backgroundColor: '#BB2427',
                         borderRadius: 8,
                         paddingVertical: 15,
                         justifyContent: 'center',
                         alignItems: 'center'
                      }}
                   >
                      <Text style={{
                         color: '#fff', 
                         fontSize: 16, 
                         fontWeight: 'bold'
                      }}>
                         Repair Disini
                      </Text>
                   </TouchableOpacity >
                   </View>               
             </KeyboardAvoidingView>
          </ScrollView>
       </View>
    )
 }

 export default Detail;
 