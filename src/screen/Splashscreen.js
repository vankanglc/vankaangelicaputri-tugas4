import React, {useEffect} from 'react'
import {View, Text, Image} from 'react-native'

const Splashscreen = ({navigation, route}) => {

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('AuthNavigation')
        },1000)
    },[])
    
    return (
      <View style={{flex:1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center'}}>
        <Image
        style={{width: 150, height: 150, resizeMode: 'contain'}}
        source={require('../assets/image/splash.png')}
        />
      </View>
    )
  }

  export default Splashscreen