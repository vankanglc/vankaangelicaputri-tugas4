import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {CheckBox} from 'react-native-elements';

import {
    View, 
    Text, 
    Image,
    ScrollView,
    KeyboardAvoidingView,
    TextInput, 
    TouchableOpacity, 
    StyleSheet,
    Dimensions
} from 'react-native'

const Summary = ({
    navigation,
    route
 }) => {
    return(
       <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
          <ScrollView //component yang digunakan agar tampilan kita bisa discroll
             showsVerticalScrollIndicator={false}
             contentContainerStyle={{paddingBottom: 10}}>
            <View style= {{marginHorizontal: 20,}}>
                <View style={styles.kotakCustomer}>
                    <Text style={{color: '#737373'}}>Data Customer </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        Agil Bani   (0813763476) </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        Jl. Perumnas, Condong catur, Sleman, Yogyakarta </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        gantengdoang@dipanggang.com </Text>
                </View>
                <View style={styles.kotakAlamat}>
                    <Text style={{color: '#737373'}}>Alamat Outlet Tujuan </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        Jack Repair - Seturan   (027-343457) </Text>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        Jl. Affandi No 18, Sleman, Yogyakarta </Text>
                </View>
                <View style={styles.kotakBarang}>
                    <Text style={{color: '#737373'}}>Barang </Text>
                <View style= {{justifyContent: 'space-between', flexDirection: 'row'}}>
                    <Image source={require('../assets/image/produk.png')}
                    style={styles.productImage}/>
                    <View style={{
                        width: '80%',
                        marginLeft: 15,
                        paddingVertical: 23, 
                        justifyContent: 'space-evenly',
                    }}>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        New Balance - Pink Abu - 40
                    </Text>
                    <Text style={{color: '#737373'}}>Cuci Sepatu </Text>
                    <Text style={{color: '#737373'}}>Note </Text>
                    </View>
                </View>
                </View>
                   <TouchableOpacity onPress={() => navigation.navigate('Berhasil')} 
                      style={{
                         width: '100%',
                         marginTop: 30,
                         backgroundColor: '#BB2427',
                         borderRadius: 8,
                         paddingVertical: 15,
                         justifyContent: 'center',
                         alignItems: 'center'
                      }}>
                      <Text style={{
                         color: '#fff', 
                         fontSize: 16, 
                         fontWeight: 'bold'
                      }}>
                         Reservasi Sekarang
                      </Text>
                   </TouchableOpacity >
                </View>            
          </ScrollView>
       </View>
    )
 }

 const styles = StyleSheet.create({
    kotakBarang: {
        backgroundColor: '#fff',
        borderRadius: 9,
        marginBottom: 5,
        marginTop: 9,
        elevation: 3,
    },
    productImage: {
        width: 84,
        height: 84,
        marginLeft: 14,
        marginVertical: 24,
    },
    kotakAlamat: {
        width: 84,
        height: 84,
        marginLeft: 14,
        marginVertical: 24,
    },
    kotakCustomer: {
        width: 84,
        height: 84,
        marginLeft: 14,
        marginVertical: 24,
    },
 })

 export default Summary;