import React, {useState} from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {CheckBox} from 'react-native-elements';

import {
    View, 
    Text, 
    Image,
    ScrollView,
    KeyboardAvoidingView,
    TextInput, 
    TouchableOpacity, 
    StyleSheet,
    Dimensions
} from 'react-native'

const Formulir = ({
    navigation,
    route
 }) => {
    const [isSelected, setSelection] = useState(false);
    const [isChecked, setCheck] = useState(false);

    return(
       <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
          <ScrollView //component yang digunakan agar tampilan kita bisa discroll
             showsVerticalScrollIndicator={false}
             contentContainerStyle={{paddingBottom: 50}}
          >
             <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                behavior='padding' //tampilan form atau text input
                enabled
                keyboardVerticalOffset={-500}
             >
                <View style={{                  
                   width: '100%',
                   backgroundColor: '#fff',
                   borderTopLeftRadius: 19,
                   borderTopRightRadius: 19,
                   paddingHorizontal: 20,
                   paddingTop: 38,
                   marginTop: -125
                }}>
                   <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Nama Toko
                   </Text> 
                   <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                      placeholder='Masukkan Nama Toko' 
                      style={{
                         marginTop: 15,
                         marginBottom: 15,
                         width: '100%',
                         borderRadius: 8,
                         backgroundColor: '#F6F8FF',
                         paddingHorizontal: 10
                      }}
                    />
                    <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Alamat
                   </Text> 
                   <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                      placeholder='Masukkan Alamat' 
                      style={{
                         marginTop: 15,
                         marginBottom: 15,
                         width: '100%',
                         borderRadius: 8,
                         backgroundColor: '#F6F8FF',
                         paddingHorizontal: 10
                      }}
                    />
                    <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Status Buka Tutup
                   </Text> 
                   <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                      placeholder='Cth : S, M, L / 39,40' 
                      style={{
                         marginTop: 15,
                         marginBottom: 15,
                         width: '100%',
                         borderRadius: 8,
                         backgroundColor: '#F6F8FF',
                         paddingHorizontal: 10
                      }}
                    />
                    <Text style={{color: 'red', fontWeight: 'bold'}}>
                      Gambar Toko
                   </Text> 
                   <View style={{
                    width: '25%',
                    height: '10%',
                    borderRadius: 8,
                    borderWidth: 1,
                    borderColor: '#BB2427',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 11,
                   }}>
                    <Image 
                        source={require('../assets/icon/Camera.png')}
                        style={{
                            width: 20,
                            height: 18                         
                        }}></Image>
                      <Text style={{
                        color: '#BB2427',
                        fontSize: 12,
                        marginTop: 10,                    
                      }}>Add Photo</Text>
                   </View>
                   <TouchableOpacity onPress={() => navigation.navigate('Keranjang')} 
                      style={{
                         width: '100%',
                         marginTop: 30,
                         backgroundColor: '#BB2427',
                         borderRadius: 8,
                         paddingVertical: 15,
                         justifyContent: 'center',
                         alignItems: 'center'
                      }}
                   >
                      <Text style={{
                         color: '#fff', 
                         fontSize: 16, 
                         fontWeight: 'bold'
                      }}>
                         Simpan
                      </Text>
                   </TouchableOpacity >
                   </View>            
             </KeyboardAvoidingView>
          </ScrollView>
       </View>
    )
 }
 export default Formulir;