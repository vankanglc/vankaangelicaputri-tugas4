import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';

const EditProfile = ({navigation}) => {
  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <View>
          <View style={styles.kotakSummary}>
            <Image
              source={require('../assets/image/ProfPic.png')}
              style={styles.imagePP}
            />
            <TouchableOpacity>
              <View style={{flexDirection: 'row', marginTop: 20}}>
                <Image
                  source={require('../assets/icon/edit.png')}
                  style={{width: 24, height: 24}}></Image>
                <Text style={styles.edit}>Edit Foto</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={{marginHorizontal: 19}}>
              <Text style={{color: 'red', fontWeight: '600'}}>Nama</Text>
              <TextInput
                placeholder="Enter your name"
                style={{
                  marginTop: 15,
                  width: '100%',
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
                keyboardType="email-address"
              />
              <Text style={{color: 'red', fontWeight: '600', marginTop: 15}}>
                Email
              </Text>
              <TextInput
                placeholder="Youremail@mail.com"
                style={{
                  marginTop: 15,
                  width: '100%',
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
                keyboardType="email-address"
              />
              <Text style={{color: 'red', fontWeight: '600', marginTop: 15}}>
                No hp
              </Text>
              <TextInput
                placeholder="Enter number"
                style={{
                  marginTop: 15,
                  width: '100%',
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
                keyboardType="numeric"
              />
              <TouchableOpacity
                style={styles.simpanButton}
                onPress={() => navigation.navigate('Profile')}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 16.3,
                    fontWeight: 'bold',
                  }}>
                  Simpan
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  kotakSummary: {
    backgroundColor: '#fff',
    marginTop: 3,
    paddingTop: 52,
    alignItems: 'center',
    paddingBottom: 40,
  },
  imagePP: {
    width: 95,
    height: 95,
  },
  edit: {
    color: '#3A4BE0',
    fontSize: 18,
    fontWeight: '500',
    marginLeft: 8,
  },
  simpanButton: {
    width: '100%',
    marginTop: 35,
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
});

export default EditProfile;
