import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {CheckBox} from 'react-native-elements';

import {
    View, 
    Text, 
    Image,
    ScrollView,
    KeyboardAvoidingView,
    TextInput, 
    TouchableOpacity, 
    StyleSheet,
    Dimensions
} from 'react-native'

const Keranjang = ({
    navigation,
    route
 }) => {
    return(
       <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
          <ScrollView //component yang digunakan agar tampilan kita bisa discroll
             showsVerticalScrollIndicator={false}
             contentContainerStyle={{paddingBottom: 10}}>
            <View style= {{marginHorizontal: 20,}}>
                <TouchableOpacity style={styles.kotakKeranjang}
                    onPress={() =>
                    navigation.navigate('Summary')}>
                <View style= {{justifyContent: 'space-between', flexDirection: 'row'}}>
                    <Image source={require('../assets/image/produk.png')}
                    style={styles.productImage}/>
                    <View style={{
                        width: '80%',
                        marginLeft: 15,
                        paddingVertical: 23, 
                        justifyContent: 'space-evenly',
                    }}>
                    <Text style={{color: '#000', fontWeight: '500'}}>
                        New Balance - Pink Abu - 40
                    </Text>
                    <Text style={{color: '#737373'}}>Cuci Sepatu </Text>
                    <Text style={{color: '#737373'}}>Note </Text>
                    </View>
                </View>
                </TouchableOpacity>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 40,
                }}>
                <Image source= {require('../assets/icon/Plus.png')}
                    style={{ width: 20, height: 20, marginRight: 6,}}></Image>
                    <Text style={{color: '#BB2427', fontWeight:'700', fontSize: 14}}>
                        Tambah Barang
                    </Text>
                    </View>
                   <TouchableOpacity onPress={() => navigation.navigate('Summary')} 
                      style={{
                         width: '100%',
                         marginTop: 30,
                         backgroundColor: '#BB2427',
                         borderRadius: 8,
                         paddingVertical: 15,
                         justifyContent: 'center',
                         alignItems: 'center'
                      }}
                   >
                      <Text style={{
                         color: '#fff', 
                         fontSize: 16, 
                         fontWeight: 'bold'
                      }}>
                         Selanjutnya
                      </Text>
                   </TouchableOpacity >
                </View>            
          </ScrollView>
       </View>
    )
 }

 const styles = StyleSheet.create({
    kotakKeranjang: {
        backgroundColor: '#fff',
        borderRadius: 9,
        marginBottom: 5,
        marginTop: 9,
        elevation: 3,
    },
    productImage: {
        width: 84,
        height: 84,
        marginLeft: 14,
        marginVertical: 24,
    },
 })

 export default Keranjang;