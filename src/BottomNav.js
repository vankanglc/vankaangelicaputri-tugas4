import * as React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/dist/Ionicons'

import Home from './screen/Home';
import Transaksi from './screen/Transaksi';
import Profile from './screen/Profile';

const Tab = createBottomTabNavigator();

export default function BottomNav(){
    return(
            <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;
                    let rn = route.name;

                    if (rn === 'Home'){
                        iconName = focused ? 'home' : 'home-outline';
                    } else if (rn === 'Transaction'){
                        iconName = focused 
                        ? 'ion-document-text'
                        : 'ios-document-text-outline';
                    } else if (rn === 'Profile'){
                        iconName = focused? 'person' : 'person-outline';
                    }
                    return <Ionicons name ={iconName} size={22} color="#BB2427" />
                },
            })}
            tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'grey',
                labelStyle: { paddingBottom: 10, fontSize: 10},
                style: {padding: 10, height: 70}
            }}
            >
            <Tab.Screen name="Home" component={Home}/>
            <Tab.Screen name="Transaction" component={Transaksi}/>
            <Tab.Screen name="Profile" component={Profile}/>
            </Tab.Navigator>
    )
}

