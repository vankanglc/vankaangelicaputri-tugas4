import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Login from './screen/Login';
import Register from './screen/Register';
import Home from './screen/Home';
import Detail from './screen/Detail';
import Formulir from './screen/Formulir';
import Keranjang from './screen/Keranjang';
import Summary from './screen/Summary';
import Berhasil from './screen/Berhasil';
import Transaksi from './screen/Transaksi';
import DetailTransaksi from './screen/DetailTransaksi';
import Profile from './screen/Profile';
import BottomNav from './BottomNav';

const Stack = createNativeStackNavigator();

function AuthNavigation(){
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="Login" component={Login}/>
            <Stack.Screen name="Register" component={Register}/>
            <Stack.Screen name="Home" component={BottomNav}/>
            <Stack.Screen name="Detail" component={Detail}/>
            <Stack.Screen name="Formulir" component={Formulir}/>
            <Stack.Screen name="Keranjang" component={Keranjang}/>
            <Stack.Screen name="Summary" component={Summary}/>
            <Stack.Screen name="Berhasil" component={Berhasil}/> 
            <Stack.Screen name="Transaksi" component={Transaksi}/>
            <Stack.Screen name="DetailTransaksi" component={DetailTransaksi}/>
            <Stack.Screen name="Profile" component={Profile}/>
        </Stack.Navigator>
    )
}

  export default AuthNavigation;